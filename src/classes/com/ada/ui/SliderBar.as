package com.ada.ui {
	import com.ada.assets.Slider;
	import com.ada.assets.Track;
	import com.ada.events.SelectEvent;
	import com.greensock.TweenLite;
	import com.greensock.easing.ExpoOut;

	import flash.display.*;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	[Event(name="select", type="com.ada.events.SelectEvent")]
	[Event(name="scroll", type="flash.events.Event")]
	/*******************************************************************************
	* @author: WhoIN Lee
	* who-in.lee@com.com
	*******************************************************************************/
	public class SliderBar extends Sprite {

		public static const SCROLL			:String = "scroll";
		public static const SELECTED_X_ARR	:Array = [0, 58, 176, 293, 409, 555, 668];
		
		private static const INIT_THUMB_X	:Number = 0;
		private static const INIT_THUMB_Y	:Number = 0;
		private static const TRACK_W		:Number = 718;	//597
		
		private var _track:Sprite;
		private var _slider:Sprite;
		
		private var _minimum:Number = 0;
		private var _maximum:Number = 100;
		private var _mouseDown:Boolean = false;
		
		private var __value:Number = 0;	//0 to 100 scale
		public function get value():Number {
			__value =  (_slider.x - INIT_THUMB_X)*(_maximum - _minimum)/(TRACK_W);
			return __value;
		}
		
		private var __sliderX:Number = -(SELECTED_X_ARR[1] - SELECTED_X_ARR[0]);
		public function get sliderX():Number {
			__sliderX = _slider.x;
			return __sliderX;
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Constructor
		////////////////////////////////////////////////////////////////////////////
		public function SliderBar() 
		{
			addEventListener(Event.ADDED_TO_STAGE, configUI);
		}
		
		private function configUI(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, configUI);
			
			_track = new Track();
			addChild(_track);
			_track.mouseEnabled = true;
			_track.mouseChildren = false;
//			_track.buttonMode = true;
			_track.addEventListener(MouseEvent.CLICK, onGutterClickHandler);
					
			_slider = new Slider();
			_slider.x = INIT_THUMB_X;
			_slider.y = INIT_THUMB_Y;
			addChild(_slider);
			_slider.buttonMode = _slider.mouseEnabled = true;
			_slider.mouseChildren = false;
			_slider.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
			_slider.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
			
			reset();
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Public Methods
		////////////////////////////////////////////////////////////////////////////
		public function setScrollProperties(minimum:Number, maximum:Number):void 
		{
			_minimum = minimum;
			_maximum = maximum;
		}
		
		public function reset():void
		{
			__value = 0;
			_slider.x = INIT_THUMB_X;
		}
		
		private function snapToTheClosest():void
		{
			var targetX:Number;
			var halfX:Number;
			var selectedIndex:uint;
			//-- slider snaps to the closest selected x position
			if (mouseX < SELECTED_X_ARR[1]) {
				halfX = SELECTED_X_ARR[0] + (SELECTED_X_ARR[1] - SELECTED_X_ARR[0])/2;
				if (mouseX < halfX) {
					targetX = SELECTED_X_ARR[0];
					selectedIndex = 0;
				} else {
					targetX = SELECTED_X_ARR[1];
					selectedIndex = 1;
				}
			} else if (mouseX < SELECTED_X_ARR[2]) {
				halfX = SELECTED_X_ARR[1] + (SELECTED_X_ARR[2] - SELECTED_X_ARR[1])/2;
				if (mouseX < halfX) {
					targetX = SELECTED_X_ARR[1];
					selectedIndex = 1;
				} else {
					targetX = SELECTED_X_ARR[2];
					selectedIndex = 2;
				}
			} else if (mouseX < SELECTED_X_ARR[3]) {
				halfX = SELECTED_X_ARR[2] + (SELECTED_X_ARR[3] - SELECTED_X_ARR[2])/2;
				if (mouseX < halfX) {
					targetX = SELECTED_X_ARR[2];
					selectedIndex = 2;
				} else {
					targetX = SELECTED_X_ARR[3];
					selectedIndex = 3;
				}
			} else if (mouseX < SELECTED_X_ARR[4]) {
				halfX = SELECTED_X_ARR[3] + (SELECTED_X_ARR[4] - SELECTED_X_ARR[3])/2;
				if (mouseX < halfX) {
					targetX = SELECTED_X_ARR[3];
					selectedIndex = 3;
				} else {
					targetX = SELECTED_X_ARR[4];
					selectedIndex = 4;
				}
			} else if (mouseX < SELECTED_X_ARR[5]){
				halfX = SELECTED_X_ARR[4] + (SELECTED_X_ARR[5] - SELECTED_X_ARR[4])/2;
				if (mouseX < halfX) {
					targetX = SELECTED_X_ARR[4];
					selectedIndex = 4;
				} else {
					targetX = SELECTED_X_ARR[5];
					selectedIndex = 5;
				}
			} else if (mouseX < SELECTED_X_ARR[6]){
				halfX = SELECTED_X_ARR[5] + (SELECTED_X_ARR[6] - SELECTED_X_ARR[5])/2;
				if (mouseX < halfX) {
					targetX = SELECTED_X_ARR[5];
					selectedIndex = 5;
				} else {
					targetX = SELECTED_X_ARR[6];
					selectedIndex = 6;
				}
			} else {
				targetX = SELECTED_X_ARR[6];
				selectedIndex = 6;
			}
			
			TweenLite.killTweensOf(_slider);
			TweenLite.to(_slider, .25, {x:targetX, ease:ExpoOut});
			
			dispatchEvent(new SelectEvent(SelectEvent.SELECT, selectedIndex));
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onMouseDownHandler(event:MouseEvent):void {
			//trace("onMouseDownHandler, mouseX is " + mouseX);
			_mouseDown = true;
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			_slider.startDrag(true, new Rectangle(INIT_THUMB_X, INIT_THUMB_Y, INIT_THUMB_X + TRACK_W, INIT_THUMB_Y));
		}
		
		private function onMouseUpHandler(event:MouseEvent):void {
			//trace("onMouseUpHandler, mouseX is " + mouseX);
			//trace(_slider.dropTarget.name);
			//trace(event.target.name);
			stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
			
			_mouseDown = false;
			_slider.stopDrag();
			snapToTheClosest();
		}

		private function onMouseMoveHandler(event:MouseEvent):void {
			//trace("onMouseMoveHandler");
			
			if (_mouseDown) {
				dispatchEvent(new Event(SCROLL));
			}
		}
		
		private function onGutterClickHandler(event:MouseEvent):void {
			snapToTheClosest();
		}
	}//c
}//p