package com.ada.ui {
	import com.ada.assets.ChartPaneAsset;
	import com.ada.events.SelectEvent;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="end", type="flash.events.Event")]
	[Event(name="slide", type="flash.events.Event")]
	public class ChartPane extends Sprite {
		
		private static const SLIDER_X			:Number = 163;	//223
		private static const SLIDER_Y			:Number = 713;	//709
		private static const MENU_X				:Number = 970;
		private static const MENU_Y				:Number = 0;
		
		private static const SLIDER_X_ARR	:Array = [0, 58, 176, 293, 409, 555, 668];
		
		private static var MASK_INIT_X			:Number = 222;	//281
		private static var MASK_INIT_Y			:Number = 503;	//486
		
		public var isEnd						:Boolean = false;
				
		private var _asset						:ChartPaneAsset;

		private var _hotSpot1					:MovieClip;
		private var _hotSpot2					:MovieClip;
		private var _hotSpot3					:MovieClip;
		private var _hotSpot4					:MovieClip;
		private var _hotSpot5					:MovieClip;
		private var _hotSpot6					:MovieClip;
		private var _p0							:MovieClip;
		private var _p1							:MovieClip;
		private var _p2							:MovieClip;
		private var _p3							:MovieClip;
		private var _p4							:MovieClip;
		private var _p5							:MovieClip;
		private var _p6							:MovieClip;
		private var _mask						:MovieClip;
		private var _gradLine					:MovieClip;
//		private var _colorSpotMask				:MovieClip;
//		private var _colorSpots					:MovieClip;
		
		private var _menuPane					:MenuPane;
		private var _sliderBar					:SliderBar;
		
		
		public function ChartPane() {
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			configUI();
		}
		
		private function configUI() : void
		{
			_asset = new ChartPaneAsset();
			addChild(_asset);
			
			_hotSpot1 = _asset.hotSpot1;
			_hotSpot2 = _asset.hotSpot2;
			_hotSpot3 = _asset.hotSpot3;
			_hotSpot4 = _asset.hotSpot4;
			_hotSpot5 = _asset.hotSpot5;
			_hotSpot6 = _asset.hotSpot6;
			
			_mask = _asset.maskMC;
			_gradLine = _asset.gradLine;
			
			_mask.cacheAsBitmap = true;
			_gradLine.cacheAsBitmap = true;
			_gradLine.mask = _mask;
				
			_sliderBar = new SliderBar();
			_sliderBar.x = SLIDER_X;
			_sliderBar.y = SLIDER_Y;
			addChild(_sliderBar);
			_sliderBar.addEventListener("scroll", onScroll);
			_sliderBar.addEventListener("select", onHotspotSelected);
			
			_menuPane = new MenuPane();
			_menuPane.x = MENU_X;
			_menuPane.y = MENU_Y;
			addChild(_menuPane);
			
			_p0 = _asset.p0;
			_p1 = _asset.p1; 
			_p2 = _asset.p2; 
			_p3 = _asset.p3;
			_p4 = _asset.p4; 
			_p5 = _asset.p5;
			_p6 = _asset.p6;
			
			MASK_INIT_X = _p0.x;
			MASK_INIT_Y = _p0.y;
			
//			_colorSpotMask = _asset.colorSpotMask;
//			_colorSpots = _asset.colorSpots;
//			_colorSpotMask.cacheAsBitmap = true;
//			_colorSpots.cacheAsBitmap = true;
//			_colorSpots.mask = _colorSpotMask;
			
			reset();
		}
		
		public function reset():void
		{
			_mask.x = MASK_INIT_X;
			_mask.y = MASK_INIT_Y;
			
//			_colorSpotMask.x = _mask.x;
//			_colorSpotMask.y = _mask.y;
			_hotSpot1.gotoAndStop(1);
			_hotSpot2.gotoAndStop(1);
			_hotSpot3.gotoAndStop(1);
			_hotSpot4.gotoAndStop(1);
			_hotSpot5.gotoAndStop(1);
			_hotSpot6.gotoAndStop(1);
			
			_sliderBar.reset();
			_menuPane.reset();
			
			isEnd = false;
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onScroll(e:Event):void
		{
			//trace("INFO ChartPane :: onScroll");
			
			var sliderX:Number = _sliderBar.sliderX;
//			trace("INFO ChartPane :: onScroll, sliderX is " + sliderX);
			
			_mask.x = MASK_INIT_X + sliderX;
			_mask.y = MASK_INIT_Y - sliderX * Math.tan(Math.PI*29/180);
			
//			_colorSpotMask.x = _mask.x;
//			_colorSpotMask.y = _mask.y;
			
			var xOffset:Number = 20;
			if (sliderX < SLIDER_X_ARR[1]-25) {
				_menuPane.select(0, .1);
				_hotSpot1.gotoAndStop(1);
				_hotSpot2.gotoAndStop(1);
				_hotSpot3.gotoAndStop(1);
				_hotSpot4.gotoAndStop(1);
				_hotSpot5.gotoAndStop(1);
				_hotSpot6.gotoAndStop(1);
			} else if ( (sliderX > SLIDER_X_ARR[1]-xOffset) && (sliderX < SLIDER_X_ARR[1]+xOffset)) {
				_menuPane.select(1, .1);
				_hotSpot1.gotoAndStop(2);
				_hotSpot2.gotoAndStop(1);
				_hotSpot3.gotoAndStop(1);
				_hotSpot4.gotoAndStop(1);
				_hotSpot5.gotoAndStop(1);
				_hotSpot6.gotoAndStop(1);
			} else if ( (sliderX > SLIDER_X_ARR[2]-xOffset) && (sliderX < SLIDER_X_ARR[2]+xOffset)) {
				_menuPane.select(2, .1);
				_hotSpot1.gotoAndStop(2);
				_hotSpot2.gotoAndStop(2);
				_hotSpot3.gotoAndStop(1);
				_hotSpot4.gotoAndStop(1);
				_hotSpot5.gotoAndStop(1);
				_hotSpot6.gotoAndStop(1);
			} else if ( (sliderX > SLIDER_X_ARR[3]-xOffset) && (sliderX < SLIDER_X_ARR[3]+xOffset)) {
				_menuPane.select(3, .1);
				_hotSpot1.gotoAndStop(2);
				_hotSpot2.gotoAndStop(2);
				_hotSpot3.gotoAndStop(2);
				_hotSpot4.gotoAndStop(1);
				_hotSpot5.gotoAndStop(1);
				_hotSpot6.gotoAndStop(1);
			} else if ( (sliderX > SLIDER_X_ARR[4]-xOffset) && (sliderX < SLIDER_X_ARR[4]+25)) {
				_menuPane.select(4, .1);
				_hotSpot1.gotoAndStop(2);
				_hotSpot2.gotoAndStop(2);
				_hotSpot3.gotoAndStop(2);
				_hotSpot4.gotoAndStop(2);
				_hotSpot5.gotoAndStop(1);
				_hotSpot6.gotoAndStop(1);
			} else if ( (sliderX > SLIDER_X_ARR[5]-xOffset) && (sliderX < SLIDER_X_ARR[5]+25)) {
				_menuPane.select(5, .1);
				_hotSpot1.gotoAndStop(2);
				_hotSpot2.gotoAndStop(2);
				_hotSpot3.gotoAndStop(2);
				_hotSpot4.gotoAndStop(2);
				_hotSpot5.gotoAndStop(2);
				_hotSpot6.gotoAndStop(1);
			} else if (sliderX > SLIDER_X_ARR[6]-25) {
				_menuPane.select(6, .1);
				_hotSpot1.gotoAndStop(2);
				_hotSpot2.gotoAndStop(2);
				_hotSpot3.gotoAndStop(2);
				_hotSpot4.gotoAndStop(2);
				_hotSpot5.gotoAndStop(2);
				_hotSpot6.gotoAndStop(2);
			}
		}
		
		private function onHotspotSelected(e:SelectEvent):void
		{
			var targetIndex:uint = e.selectedIndex;
			if (targetIndex == 6) {
				isEnd = true;
				dispatchEvent(new Event("end"));
			} else {
				isEnd = false;
				dispatchEvent(new Event("slide"));
			}
			
			_mask.x = this["_p" + targetIndex].x;
			_mask.y = this["_p" + targetIndex].y;
			
//			_colorSpotMask.x = _mask.x;
//			_colorSpotMask.y = _mask.y;
			
			_menuPane.select(targetIndex);
			
			switch (targetIndex) {
				case 0:
					_hotSpot1.gotoAndStop(1);
					_hotSpot2.gotoAndStop(1);
					_hotSpot3.gotoAndStop(1);
					_hotSpot4.gotoAndStop(1);
					_hotSpot5.gotoAndStop(1);
					_hotSpot6.gotoAndStop(1);
					break;
				case 1:
					_hotSpot1.gotoAndStop(2);
					_hotSpot2.gotoAndStop(1);
					_hotSpot3.gotoAndStop(1);
					_hotSpot4.gotoAndStop(1);
					_hotSpot5.gotoAndStop(1);
					_hotSpot6.gotoAndStop(1);
					break;
				case 2:
					_hotSpot1.gotoAndStop(2);
					_hotSpot2.gotoAndStop(2);
					_hotSpot3.gotoAndStop(1);
					_hotSpot4.gotoAndStop(1);
					_hotSpot5.gotoAndStop(1);
					_hotSpot6.gotoAndStop(1);
					break;
				case 3:
					_hotSpot1.gotoAndStop(2);
					_hotSpot2.gotoAndStop(2);
					_hotSpot3.gotoAndStop(2);
					_hotSpot4.gotoAndStop(1);
					_hotSpot5.gotoAndStop(1);
					_hotSpot6.gotoAndStop(1);
					break;
				case 4:
					_hotSpot1.gotoAndStop(2);
					_hotSpot2.gotoAndStop(2);
					_hotSpot3.gotoAndStop(2);
					_hotSpot4.gotoAndStop(2);
					_hotSpot5.gotoAndStop(1);
					_hotSpot6.gotoAndStop(1);
					break;
				case 5:
					_hotSpot1.gotoAndStop(2);
					_hotSpot2.gotoAndStop(2);
					_hotSpot3.gotoAndStop(2);
					_hotSpot4.gotoAndStop(2);
					_hotSpot5.gotoAndStop(2);
					_hotSpot6.gotoAndStop(1);
					break;
				case 6:
					_hotSpot1.gotoAndStop(2);
					_hotSpot2.gotoAndStop(2);
					_hotSpot3.gotoAndStop(2);
					_hotSpot4.gotoAndStop(2);
					_hotSpot5.gotoAndStop(2);
					_hotSpot6.gotoAndStop(2);
					break;
			}
			

		}
	}//c
}//p