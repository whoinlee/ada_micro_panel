package com.ada.ui {
	import com.ada.assets.MenuPaneAsset;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	import com.greensock.easing.Quint;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class MenuPane extends Sprite {
		
		private static const INDICATOR_INIT_Y	:Number = 453;
		private static const INDICATOR_TEXT_OFFSET	:Number = 18;
		private static const SELECTED_COLORS	:Array = [0xffffff, 0x0cadc0, 0x50af32, 0xf0e621, 0xf39325, 0xea5b0c, 0xe7302a];

		private var _asset						:MenuPaneAsset;
		private var _text1						:MovieClip;
		private var _text2						:MovieClip;
		private var _text3						:MovieClip;
		private var _text4						:MovieClip;
		private var _text5						:MovieClip;
		private var _text6						:MovieClip;
		private var _indicator					:MovieClip;
		
		private var _selectedText				:MovieClip = null;
		private var _selectedIndex				:uint = 0;

		
		public function MenuPane() {
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			configUI();
		}
		
		private function configUI() : void
		{
			_asset = new MenuPaneAsset();
			addChild(_asset);
			
			_text1 = _asset.text1;
			_text2 = _asset.text2;
			_text3 = _asset.text3;
			_text4 = _asset.text4;
			_text5 = _asset.text5;
			_text6 = _asset.text6;
			
			_indicator = _asset.indicator;
			_indicator.bkg.alpha = .95;
			_indicator.alpha = 0;
			
			reset();
		}
		
		public function reset():void
		{
			_text1.gotoAndStop(1);
			_text2.gotoAndStop(1);
			_text3.gotoAndStop(1);
			_text4.gotoAndStop(1);
			_text5.gotoAndStop(1);
			_text6.gotoAndStop(1);
			_selectedText = null;
			_selectedIndex = 0;
			
			_indicator.y = INDICATOR_INIT_Y;
			_indicator.alpha = 0;
			TweenLite.to(_indicator.bkg, 0, {ease:Quad.easeOut, tint:SELECTED_COLORS[_selectedIndex]});
		}
		
		public function select(index:uint = 0, duration:Number = .3 ):void
		{
			if (_selectedIndex != index) {
//				trace("INFO MenuPane :: select, ever? " + index);
				
				if (_selectedText != null) _selectedText.gotoAndStop(1);
				_selectedIndex = index;
				var targetY:Number;
				var targetAlpha:Number = 1;
				if (index != 0) {
					_selectedText = this["_text" + _selectedIndex];
					targetY = _selectedText.y - INDICATOR_TEXT_OFFSET;
					targetAlpha = 1;
				} else {
					_selectedText = null;
					targetY = INDICATOR_INIT_Y;
					targetAlpha = 0;
					targetAlpha = 0;
				}
				
				changeTextColor();
				TweenLite.to(_indicator.bkg, duration + .1, {tint:SELECTED_COLORS[_selectedIndex]});
				TweenLite.to(_indicator, duration, {y:targetY, alpha:targetAlpha, ease:Quad.easeOut});
			}
		}
		
		private function changeTextColor():void
		{
			if (_selectedText) _selectedText.gotoAndStop(2);
		}
		
	}//c
}//p