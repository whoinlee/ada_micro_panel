package com.ada.view {
	import com.ada.assets.HomeButton;
	import com.ada.assets.NextButton;
	import com.ada.assets.Page1Content;
	import com.ada.ui.ChartPane;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="back", type="flash.events.Event")]
	[Event(name="next", type="flash.events.Event")]
	public class Page1View extends BaseView {

		private static const PANE_X				:Number = 296;
		private static const PANE_Y				:Number = 136;
		private static const BUTTON1_X			:Number = 73;
		private static const BUTTON2_X			:Number = 1646;
		private static const BUTTON_Y			:Number = 492;
		
		private var _content					:MovieClip;
		private var _backBt						:Sprite;
		private var _nextBt						:Sprite;
		private var _chartPane					:ChartPane;
		
		
		public function Page1View() {
			super();
			viewName = "Page1";
		}
		
		override protected function configUI() : void
		{
			_content = new Page1Content();
			addChild(_content);
			
			_chartPane = new ChartPane();
			_chartPane.x = PANE_X;
			_chartPane.y = PANE_Y;
			addChild(_chartPane);
			_chartPane.addEventListener("slide", showBack);
			_chartPane.addEventListener("end", showNext);
			
			_backBt = new HomeButton();
			_backBt.x = BUTTON1_X;
			_backBt.y = BUTTON_Y;
			addChild(_backBt);
			
			_backBt.buttonMode = _backBt.mouseEnabled = true;
			_backBt.mouseChildren = false;
			
			_nextBt = new NextButton();
			_nextBt.x = BUTTON2_X;
			_nextBt.y = BUTTON_Y;
			addChild(_nextBt);

			_nextBt.buttonMode = _nextBt.mouseEnabled = true;
			_nextBt.mouseChildren = false;
			_nextBt.visible = false;
		}
		
		public function reset(isChartReset:Boolean = true):void
		{
			//_content.gotoAndStop(1);
			activateBack();
			deactivateNext();
			if (isChartReset) _chartPane.reset();
		}
		
		public function activateButtons():void
		{
			if (_chartPane.isEnd) {
				activateNext();
				deactivateBack();
			} else {
				activateBack();
				deactivateNext();
			}
		}
		
		public function showRef():void
		{
			if (_content.currentFrame == 1)
			_content.gotoAndPlay("showRef");
		}
		
		public function hideRef():void
		{
			_content.gotoAndStop(1);
		}
		
		private function activateNext():void
		{
			if (!_nextBt.visible) {
				_nextBt.alpha = 1;
				_nextBt.visible = true;
			}
			_nextBt.addEventListener(MouseEvent.CLICK, onNextRequested);
		}
		
		private function deactivateNext():void
		{
			_nextBt.removeEventListener(MouseEvent.CLICK, onNextRequested);
			if (_nextBt.visible)
			TweenLite.to(_nextBt, .1, {alpha:0, ease:Quad.easeOut, onComplete:hideNext});
		}
		
		private function hideNext():void
		{
			_nextBt.visible = false;
		}
		
		private function activateBack():void
		{
			if (!_backBt.visible) {
				_backBt.alpha = 1;
				_backBt.visible = true;
			}
			_backBt.addEventListener(MouseEvent.CLICK, onBackRequested);
		}
		
		private function deactivateBack():void
		{
			_backBt.removeEventListener(MouseEvent.CLICK, onBackRequested);
			if (_backBt.visible)
			TweenLite.to(_backBt, .1, {alpha:0, ease:Quad.easeOut, onComplete:hideBack});
		}
		
		private function hideBack():void
		{
			_backBt.visible = false;
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onBackRequested(e:MouseEvent):void
		{
			deactivateBack();
			hideRef();
			dispatchEvent(new Event("back"));
		}
		
		private function onNextRequested(e:MouseEvent):void
		{
			deactivateNext();
			dispatchEvent(new Event("next"));
		}
		
		private function showBack(e:Event = null):void
		{
			if (!_backBt.visible) {
				deactivateNext();
					
				_backBt.alpha = 0;
				_backBt.visible = true;	
				TweenLite.to(_backBt, .5, {alpha:1, ease:Quad.easeOut, onComplete:activateBack});
			}
		}
		
		private function showNext(e:Event = null):void
		{
			if (!_nextBt.visible) {
				deactivateBack();
				
				_nextBt.alpha = 0;
				_nextBt.visible = true;
				TweenLite.to(_nextBt, .5, {alpha:1, ease:Quad.easeOut, onComplete:activateNext});
			}
		}
	}//c
}//p