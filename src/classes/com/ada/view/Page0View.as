package com.ada.view {
	import com.ada.assets.Page0Content;

	import flash.display.MovieClip;
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class Page0View extends BaseView {

		private var _content					:MovieClip;
		
		
		public function Page0View() {
			super();
			viewName = "Page0";
		}
		
		override protected function configUI() : void
		{
			_content = new Page0Content();
			addChild(_content);
		}
	}//c
}//p