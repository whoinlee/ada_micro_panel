package com.ada.view {
	import com.ada.data.DataModel;

	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	public class BaseView extends Sprite {
		
		public var viewName:String = "";
		protected var _dataModel:DataModel;
		
		
		public function BaseView() {
			//trace("INFO BaseView :: ");

			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			_dataModel = DataModel.getInstance();
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			configUI();
		}
		
		protected function configUI():void {}

		public function build():void {}
		
		public function destroy():void {}
		
		public function show():void {}
		
		public function hide():void {}
	}
}