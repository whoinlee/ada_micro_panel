package com.ada.view {
	import com.ada.assets.BackButton;
	import com.ada.assets.Page2Content;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[Event(name="back", type="flash.events.Event")]
	public class Page2View extends BaseView {

		private static const BUTTON1_X			:Number = 73;
		private static const BUTTON_Y			:Number = 492;
		
		private var _content					:MovieClip;
		private var _backBt						:Sprite;
		
		
		public function Page2View() {
			super();
			viewName = "Page2";
		}
		
		override protected function configUI() : void
		{
			_content = new Page2Content();
			addChild(_content);
			
			_backBt = new BackButton();
			_backBt.x = BUTTON1_X;
			_backBt.y = BUTTON_Y;
			addChild(_backBt);
			
			_backBt.buttonMode = _backBt.mouseEnabled = true;
			_backBt.mouseChildren = false;
		}
		
		public function reset():void
		{
			//_content.gotoAndStop(1);
			activateBack();
		}
		
		public function showRef():void
		{
			if (_content.currentFrame == 1)
			_content.gotoAndPlay("showRef");
		}
		
		public function hideRef():void
		{
			_content.gotoAndStop(1);
		}
		
		private function activateBack():void
		{
			_backBt.addEventListener(MouseEvent.CLICK, onBackRequested);
		}
		
		private function deactivateBack():void
		{
			_backBt.removeEventListener(MouseEvent.CLICK, onBackRequested);
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onBackRequested(e:MouseEvent):void
		{
			deactivateBack();
			hideRef();
			dispatchEvent(new Event("back"));
		}
	}//c
}//p