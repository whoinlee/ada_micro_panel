package com.ada {
	import com.ada.assets.Copyright;
	import com.ada.data.DataModel;
	import com.ada.view.BaseView;
	import com.ada.view.Page0View;
	import com.ada.view.Page1View;
	import com.ada.view.Page2View;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;
	import com.greensock.plugins.TintPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.system.fscommand;
	import flash.ui.Mouse;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;

	/**
	 * @author WhoIN Lee : whoin@hotmail.com
	 */
	[SWF(backgroundColor="#ffffff", frameRate="30", width="1920", height="1080")]
	public class Main extends Sprite {
		
		private static const TARGET0_X	:Number = 0;
		private static const TARGET1_X	:Number = -Config.STAGE_W;
		private static const TARGET2_X	:Number = -(Config.STAGE_W*2);
		
//		private static const LOGO_X	:Number = 1484;
//		private static const LOGO_Y	:Number = 942;
		
		//-- views
		private var _page0:Page0View;
		private var _page1:Page1View;
		private var _page2:Page2View;
		
		private var _backToInitID:Number;
		private var _currentView:BaseView;
		private var _viewHolder:Sprite;
		private var _attractLoop:Loader;
		
		private var _bkgHolder:Sprite;
		private var _bkg:Loader;
		private var _copyright:Sprite;
		
		//-- singletons
		private var _dataModel:DataModel;
		
		
		public function Main() {
//			trace("INFO Main :: ");
			
			TweenPlugin.activate([TintPlugin]);

			_dataModel = DataModel.getInstance();
			_dataModel.getData(Config.CONFIG_PATH);
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e : Event) : void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.SHOW_ALL;	//for local test
			stage.showDefaultContextMenu = false;
			
			if (_dataModel.isDataReady) {
				init();
			} else {
				_dataModel.addEventListener(DataModel.DATA_READY, init);
			}
		}
		
		private function init(e:Event = null) : void 
		{
			if (Config.hideCursor) Mouse.hide();
			if (Config.fullScreen) fscommand("fullscreen","true");
			
			_bkgHolder = new Sprite();
			addChild(_bkgHolder);
			
			_bkg = new Loader();
			_bkgHolder.addChild(_bkg);
			_bkg.load(new URLRequest(Config.bkgMoviePath));
			_bkg.contentLoaderInfo.addEventListener(Event.COMPLETE, onBkgLoaded);
			
//			_copyright = new Copyright();
//			_bkgHolder.addChild(_copyright);
			
			_viewHolder = new Sprite();
			addChild(_viewHolder);
			
			buildPage0();
			buildPage1();
			buildPage2();
			initTouch();
			
			if (Config.idleTimeout > 0) {
				buildAttractLoop();
				//stage.addEventListener(MouseEvent.MOUSE_MOVE, hideAttractLoop);
				stage.addEventListener(MouseEvent.MOUSE_MOVE, resetTimer);
				_backToInitID = setTimeout(backToInit, Config.idleTimeout);
			}
			
			showAttractLoop();
		}
		
		private function buildPage0():void
		{
			_page0 = new Page0View();
			_viewHolder.addChild(_page0);
		}
		
		private function buildPage1():void
		{
			_page1 = new Page1View();
			_page1.x = Config.STAGE_W;
			_viewHolder.addChild(_page1);
			_page1.addEventListener("back", gotoPage0);
			_page1.addEventListener("next", gotoPage2);
		}
		
		private function buildPage2():void
		{
			_page2 = new Page2View();
			_page2.x = Config.STAGE_W*2;
			_viewHolder.addChild(_page2);
			_page2.addEventListener("back", gotoPage1);
		}
		
		private function buildAttractLoop():void
		{
			_attractLoop = new Loader();
			_page0.addChild(_attractLoop);
			_attractLoop.visible = false;
		}
		
		private function showAttractLoop():void
		{
			//if (!_attractLoop.visible) {
//				trace("INFO Main :: showAttractLoop");
				_attractLoop.visible = true;
				//_attractLoop.contentLoaderInfo.addEventListener(Event.COMPLETE, onAttractLoopLoaded);
     			_attractLoop.load(new URLRequest(Config.attractPath));
			//}
		}

		private function hideAttractLoop(e:MouseEvent = null):void
		{
			TweenLite.killTweensOf(showAttractLoop);
			
			if (_attractLoop.visible) {
//				trace("INFO Main :: hideAttractLoop");
				_attractLoop.visible = false;
				_attractLoop.unload();
			}
			resetTimer();
		}

		private function backToInit(e:MouseEvent = null):void
		{
//			trace("INFO Main :: backToInit");
			
			if (_currentView != _page0) {
				_page1.reset();
				_page2.reset();
				gotoPage0();
			} else {
				initTouch();
			}
			resetTimer();
		}
		
		private function resetTimer(e:MouseEvent = null):void
		{
			if (Config.idleTimeout > 0) {
				clearTimeout(_backToInitID);
				_backToInitID = setTimeout(backToInit, Config.idleTimeout);
			}
		}
		
		private function resetPage1():void
		{
			_page1.reset();
		}
		
		private function onGetToPage0():void
		{
			_currentView = _page0;
			initTouch();
		}
		
		private function onGetToPage1():void
		{
			_currentView = _page1;
			_page1.activateButtons();
			_page1.showRef();
			
			hideAttractLoop();
		}
		
		private function onGetToPage2():void
		{
			_currentView = _page2;
			_page2.reset();
			_page2.showRef();
		}
		
		private function initTouch():void
		{
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onTouchScreen);
		}
		
		private function killTouch():void
		{
			stage.removeEventListener(MouseEvent.MOUSE_DOWN, onTouchScreen);
			clearTimeout(_backToInitID);
		}
		
		
		////////////////////////////////////////////////////////////////////////////
		// Event Handlers
		////////////////////////////////////////////////////////////////////////////
		private function onTouchScreen(e:MouseEvent):void
		{
			//trace("INFO Main :: onTouchScreen");
			killTouch();
			resetPage1();
			gotoPage1();

//			hideAttractLoop();
		}
		
		private function gotoPage0(e:Event = null):void
		{
//			showAttractLoop();
			TweenLite.killTweensOf(showAttractLoop);
			var myCall : TweenLite = new TweenLite(showAttractLoop, .2, {onComplete:showAttractLoop, overwrite:false});
			TweenLite.to(_viewHolder, .5, {x:TARGET0_X, ease:Quad.easeOut, onComplete:onGetToPage0});
		}
		
		private function gotoPage1(e:Event = null):void
		{
			TweenLite.to(_viewHolder, .5, {x:TARGET1_X, ease:Quad.easeOut, onComplete:onGetToPage1});
		}
		
		private function gotoPage2(e:Event = null):void
		{
			TweenLite.to(_viewHolder, .5, {x:TARGET2_X, ease:Quad.easeOut, onComplete:onGetToPage2});
		}
		
//		private function onAttractLoopLoaded(e:Event):void
//		{
//			trace("INFO Main :: onAttractLoopLoaded");
//		}

		private function onBkgLoaded(e:Event):void
		{
//			trace("INFO Main :: onBkgLoaded");
			_copyright = new Copyright();
			_bkgHolder.addChild(_copyright);
		}
	}//c
}//p